package com.company;

class EmptyDataException extends Exception {}
class Main {

    // rzuca wyjatek EmptyDataException gdy parametr wejsciowy jest pusty

    private int getMaxFromDigits(final String digits) throws EmptyDataException {

        if(digits == null || digits.isEmpty()){
            throw new EmptyDataException();
        }

        String[] stringArray = digits.split(",");
        int[] intArray = new int[stringArray.length];

        //convert
        for(int i = 0; i < stringArray.length; i++){
            String numberAsString = stringArray[i];
            intArray[i] = Integer.parseInt(numberAsString);
        }

        //find max
        int maxValue = intArray[0];
        for(int i=1;i < intArray.length;i++){
            if(intArray[i] > maxValue){
                maxValue = intArray[i];
            }
        }

        return maxValue;
    }
    public static void main(String[] args) {
        final String listOfDigits = null;
        final Main main = new Main();
        try {
            int maxFromDigits = main.getMaxFromDigits(listOfDigits);
            System.out.printf("max(%s) = %d \n", listOfDigits, maxFromDigits);
        } catch(EmptyDataException e) {
            System.out.println("Lista nie moze byc pusta!");
        }
    }
}
